# Mock Tepee

![](https://gitlab.com/zar3bski/mock_tepee/badges/master/pipeline.svg) 
![](https://gitlab.com/zar3bski/mock_tepee/badges/master/coverage.svg) 
![](https://img.shields.io/docker/image-size/zar3bski/mock_tepee)
![](https://img.shields.io/docker/v/zar3bski/mock_tepee)


A minimal HTTP mock server

## Features

### `Content-Type` indentification

Content Types are identified based on the syntax used in response. Currently supported types: 

* `application/json` for `{"status":1}`
* `application/xml` for `"<status>1</status>"`
* `text/plain` for `"anything else"`

## Usage

Both standalone and docker usage of this mock server require a valid conf file. Here is an example of such a JSON **config file**: 

```json
{
	"/hello":{
		"GET": {"sc":200,"response":"Hello, World!"},
		"POST": {"sc":201}
	},
	"/how":{
		"POST": {"sc":200, "response": {"status":1}}
	},
	"/are":{
		"POST": {"sc":200, "response": "<status>1</status>"}
	},
	"/you":{
		"POST": {"sc":200, "headers": {"server":"nginx"}}
	}
}
```

YAML confs can also be used. Here is the equivalent of the previous one

```yaml
/hello:
  GET: 
    sc: 200
    response: Hello, World!
  POST: 
    sc: 201
/how:
  POST:
    sc: 200
    response:
      status: 1
/are:
  POST:
    sc: 200
    response: <status>1</status>
/you:
  POST:
    sc: 200
    headers:
      server: nginx
```

Docker usage requires that files, not matter the type, are mounted on `/conf`

### Standalone

```bash
python /path/to/server.py 5000 /path/to/config.json
```

### Docker

```bash
docker run -p 8080:5000 -v /path/to/config.json:/conf:ro zar3bski/mock_tepee:amd64-latest
```
